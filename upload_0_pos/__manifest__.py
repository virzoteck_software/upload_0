# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Display products in POS Screen when quantity of product>0',
    'version': '2.0',
    'complexity': 'easy',
    'website': '',
    'description': """By installing this module,only those product will be visible in point of sale whos quantity is greater than 0""",
    'author':'Virzoteck softwares and solutions(virzoteck@gmail.com)',    
    'depends': ['point_of_sale'],
    'data': [
      
        'views/conf.xml'
      
        
    ],    
    'images': ['static/description/background.jpg',],
        'category': 'Point Of Sale',

    'installable': True,
    'auto_install': True,
    'license': 'LGPL-3',
    "price":15,
    "currency": "EUR"    
}
