odoo.define('upload_0_pos.pos_custom', function (require) {
"use strict";

var models = require('point_of_sale.models');
var _super_posmodel = models.PosModel.prototype;
models.PosModel = models.PosModel.extend({
    initialize: function (session, attributes) {
        // New code
//        var partner_model = _.find(this.models, function(model){
//            return model.model === 'res.partner';
//        });
//        partner_model.fields.push('doctor');

        var product_model = _.find(this.models, function(model){
            return model.model === 'product.product';
        });
        
        product_model.domain.push(['qty_available','>',0]);

        // Inheritance
        return _super_posmodel.initialize.call(this, session, attributes);
    },
  
    
});

});

